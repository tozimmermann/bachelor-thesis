# Bachelor Thesis - Introducing Primordial Radiative Cooling to PKDGRAV3

This repository includes the files that were used to produce the results in my bachelor thesis. 
The files are organised in two folders: `Scripts` and `Experiments`.

## Scripts
This folder contains the plotting scripts used to generate the plots in the thesis. 
These include:
- `plot_single.py`: Plot all cooling models at a single time step.
- `plot_animation.py`: Produce a video that plots the results of all cooling models every 5 timesteps.
- `plot_analytical.py`: Plot the analytical solution at a single time step.
- `plot_evolution.py`: Plot the evolution of the analytical solution at a few time steps.
- `plot_adaptive_sod.py`: Plot the results of a cooling model with the analytical solution adapted to the average temperature.
- `energy_level_diagram_hydrogen.py`: Plot the energy level diagram of hydrogen.
- The other scripts are used to plot the results of the experiments.

## Experiments 
This folder contains the files necessary to run the three experiments conducted: 
- **Cold Sod Shock Tube** ($T_{high}$ = 10 K, $T_{low}$ = 8 K)
- **Hot Sod Shock Tube** ($T_{high}$ = 16000 K, $T_{low}$ = 12800 K)
- **Pressure Equilibrium** ($T_{high}$ = 16000 K, $T_{low}$ = 2000 K)

Every experiment folder contains the following folders:
- `generate`: Contains the files to generate the initial conditions.
- `plots`: The output folder of the plots.
- `relax`: Contains the files to relax the initial conditions.
- `run`: Contains the parameter files and batch scripts to run the experiments using the different cooling models.
