#!/bin/bash
#SBATCH -J "relax_peq_left"
#SBATCH --account=uzh18
#SBATCH --constraint=mc
#SBATCH --time=04:00:00
#SBATCH --no-requeue
#SBATCH --nodes=2
#SBATCH --cpus-per-task=256
#SBATCH --ntasks-per-core=2
#SBATCH --ntasks-per-node=1
#SBATCH --kill-on-invalid-dep=yes

module load cray
module load PrgEnv-gnu
module load cray-hdf5 cray-fftw CMake cray-python hwloc
module load cpeGNU/21.12
module load GSL Boost
module load ffmpeg

export FI_CXI_RX_MATCH_MODE=software
export FI_CXI_RDZV_THRESHOLD=$((2*1048576))
export FI_CXI_REQ_BUF_SIZE=$((2*2097152))
export FI_CXI_REQ_BUF_MIN_POSTED=60
export FI_CXI_REQ_BUF_MAX_CACHED=200


lscpu
module list

echo ""; echo "START at "; date; echo ""

time srun $HOME/BA/pkdgrav3-sphcooling/build/pkdgrav3 cosmology_relax_peq_left.par

echo ""; echo "END at "; date; echo ""

cp relax_peq_left/relax_peq_left.01600 ./peq_ic_grid_isentrope_left_relaxed1600.tipsy

cd ../../scripts/

echo ""; echo "ANIMATE"; echo ""

time python animate.py relax_peq_left 1600

echo ""; echo "DONE"; echo ""
