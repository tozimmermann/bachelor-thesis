echo "Compiling IC_generator_peq.cpp"
g++ -o IC_generator_peq IC_generator_peq.cpp
echo ""

echo "Running IC_generator_peq"
./IC_generator_peq
echo ""

echo "Calculating density with PkDGrav3"
../../../../pkdgrav3-sphcooling/build/pkdgrav3 -n0 peq_n0_left.par
../../../../pkdgrav3-sphcooling/build/pkdgrav3 -n0 peq_n0_right.par
echo ""

echo "Adjusting temperature to move particles to the same isotrope"
python3 ../../scripts/adjust_isentrope.py peq_n0_left.00000 1.4
python3 ../../scripts/adjust_isentrope.py peq_n0_right.00000 1.4
echo ""

echo "Compiling txt2tipsy.cpp"
g++ -o txt2tipsy ../../scripts/txt2tipsy.cpp
echo ""

echo "Converting to tipsy format"
./txt2tipsy peq_n0_left.00000_adjusted_isentrope
./txt2tipsy peq_n0_right.00000_adjusted_isentrope
echo ""
echo "moving temporary files to temporary folder"
mkdir temporary
mv ic_peq_grid_left_256_32_32.tipsy peq_n0_left.00000 peq_n0_left.00000_adjusted_isentrope.txt temporary
mv ic_peq_grid_right_128_16_16.tipsy peq_n0_right.00000 peq_n0_right.00000_adjusted_isentrope.txt temporary

echo "Moving files"
mv peq_n0_left.00000_adjusted_isentrope.tipsy ../relax/peq_ic_grid_isentrope_left.tipsy
mv peq_n0_right.00000_adjusted_isentrope.tipsy ../relax/peq_ic_grid_isentrope_right.tipsy
#cp peq_ic_grid_isentrope_left.tipsy ~/EULER/cluster/bachelorthesis/NewSPH/pkdgrav3/run/
#cp peq_ic_grid_isentrope_right.tipsy ~/EULER/cluster/bachelorthesis/NewSPH/pkdgrav3/run/