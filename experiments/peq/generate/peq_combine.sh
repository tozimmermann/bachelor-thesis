echo "combining"
python3 peq_combine.py ../relax/peq_ic_grid_isentrope_left_relaxed1600.tipsy ../relax/peq_ic_grid_isentrope_right_relaxed1600.tipsy

echo "writing to tipsy"
g++ -o txt2tipsy ../../scripts/txt2tipsy.cpp
./txt2tipsy peq_ic_grid_isentrope_relaxed1600_combined

echo "moving temporary files to temporary folder"
mkdir temporary
mv peq_ic_grid_isentrope_relaxed1600_combined.txt temporary
cp peq_ic_grid_isentrope_relaxed1600_combined.tipsy ../run/
#cp peq_ic_grid_isentrope_relaxed1600_combined.tipsy ~/EULER/cluster/bachelorthesis/NewSPH/pkdgrav3/run/
