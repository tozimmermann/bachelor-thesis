#!/bin/bash
#SBATCH -J "peq_grackle"
#SBATCH --account=uzh18
#SBATCH --constraint=mc
#SBATCH --time=02:00:00
#SBATCH --no-requeue
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=16
#SBATCH --kill-on-invalid-dep=yes

module load cray
module load PrgEnv-gnu
module load cray-hdf5 cray-fftw CMake cray-python hwloc
module load cpeGNU/21.12
module load GSL Boost
module load ffmpeg

export FI_CXI_RX_MATCH_MODE=software
export FI_CXI_RDZV_THRESHOLD=$((2*1048576))
export FI_CXI_REQ_BUF_SIZE=$((2*2097152))
export FI_CXI_REQ_BUF_MIN_POSTED=60
export FI_CXI_REQ_BUF_MAX_CACHED=200


lscpu

# echo ""; echo "GENERATE"; echo ""
# module restore; module load gcc
# cd ../tools/; source generate.sh; cd ../run/

echo ""; echo "START at "; date; echo ""

#time srun --cpus-per-task=16 $HOME/BA/pkdgrav3-sphcooling/build/pkdgrav3 nocool.par
#time srun --cpus-per-task=16 $HOME/BA/pkdgrav3-sphcooling/build/pkdgrav3 kwh.par
#time srun --cpus-per-task=16 $HOME/BA/pkdgrav3-sphcooling/build/pkdgrav3 kwh_heat.par
#time srun --cpus-per-task=16 $HOME/BA/pkdgrav3-sphcooling/build/pkdgrav3 grackle_eq.par
time srun --cpus-per-task=16 $HOME/BA/pkdgrav3-sphcooling/build/pkdgrav3 grackle.par

echo ""; echo "END at "; date; echo ""

#echo ""; echo "PLOT"; echo ""
#
#cd ../../scripts/
#
#time python plot.py sod density 100
#time python plot.py sod pressure 100
#
#echo ""; echo "ANIMATE"; echo ""
#
#time python animate.py sod 100
#
#cd ../sod/run/

echo ""; echo "DONE"; echo ""