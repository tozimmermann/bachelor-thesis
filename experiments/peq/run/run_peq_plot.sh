#!/bin/bash
#SBATCH -J "peq_plot"
#SBATCH --account=uzh18
#SBATCH --constraint=mc
#SBATCH --time=04:00:00
#SBATCH --no-requeue
#SBATCH --nodes=2
#SBATCH --cpus-per-task=256
#SBATCH --ntasks-per-core=2
#SBATCH --ntasks-per-node=1
#SBATCH --kill-on-invalid-dep=yes

module load cray
module load PrgEnv-gnu
module load cray-hdf5 cray-fftw CMake cray-python hwloc
module load cpeGNU/21.12
module load GSL Boost
pip install python-ffmpeg
pip install scipy

export FI_CXI_RX_MATCH_MODE=software
export FI_CXI_RDZV_THRESHOLD=$((2*1048576))
export FI_CXI_REQ_BUF_SIZE=$((2*2097152))
export FI_CXI_REQ_BUF_MIN_POSTED=60
export FI_CXI_REQ_BUF_MAX_CACHED=200


lscpu

echo ""; echo "PLOT"; echo ""

cd ../../scripts/

time python plot.py $HOME/BA/ba-cooling/tests/peq peq density 1000
time python plot.py $HOME/BA/ba-cooling/tests/peq peq pressure 1000

echo ""; echo "ANIMATE"; echo ""

time python animate.py $HOME/BA/ba-cooling/tests/peq peq 1000

cd ../peq/run/

echo ""; echo "DONE"; echo ""
