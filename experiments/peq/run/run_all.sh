#!/bin/bash

sbatch run_peq_nocool.sh
sbatch run_peq_kwh.sh
sbatch run_peq_kwh_heat.sh
sbatch run_peq_grackle_eq.sh
sbatch run_peq_grackle.sh

sbatch run_peq_isentropic_nocool.sh
sbatch run_peq_isentropic_kwh.sh
sbatch run_peq_isentropic_kwh_heat.sh
sbatch run_peq_isentropic_grackle_eq.sh
sbatch run_peq_isentropic_grackle.sh