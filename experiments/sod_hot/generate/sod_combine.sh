echo "combining"
python3 sod_combine.py ../run/sod_ic_grid_isentrope_left_relaxed1600.tipsy ../run/sod_ic_grid_isentrope_right_relaxed1600.tipsy

echo "writing to tipsy"
g++ -o txt2tipsy ../../scripts/txt2tipsy.cpp
./txt2tipsy sod_ic_grid_isentrope_relaxed1600_combined

echo "moving temporary files to temporary folder"
mkdir temporary
mv sod_ic_grid_isentrope_relaxed1600_combined.txt temporary
cp sod_ic_grid_isentrope_relaxed1600_combined.tipsy ../run/
#cp sod_ic_grid_isentrope_relaxed1600_combined.tipsy ~/EULER/cluster/bachelorthesis/NewSPH/pkdgrav3/run/