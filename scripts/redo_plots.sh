#!/bin/bash

typeset -A projects
projects=(["peq"]=14400 ["peq_isentropic"]=14400 ["sod_10"]=9 ["sod_16000"]=14400 ["sod_16000_isentropic"]=14400)


for PROJECT in "${!projects[@]}"; do
    TEMP=${projects[$PROJECT]}
    
#    for TIMESTEP in 0 25 50 75 100; do
  
        python3 plot_single.py ~/ba/ba-cooling/tests/results_new ${PROJECT} density 75 $TEMP
        python3 plot_single.py ~/ba/ba-cooling/tests/results_new ${PROJECT} pressure 75 $TEMP
        python3 plot_single.py ~/ba/ba-cooling/tests/results_new ${PROJECT} temperature 75 $TEMP

#    done
done
