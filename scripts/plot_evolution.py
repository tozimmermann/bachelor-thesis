import numpy as np
import pandas as pd
from sys import argv, exit
from matplotlib import pyplot as plt
import sod

header_type = np.dtype(
    [('time', '=f8'), ('N', '=i4'), ('Dims', '=i4'), ('Ngas', '=i4'), ('Ndark', '=i4'), ('Nstar', '=i4'),
     ('pad', '=i4')])
gas_type = np.dtype(
    [('mass', '=f4'), ('x', '=f4'), ('y', '=f4'), ('z', '=f4'), ('vx', '=f4'), ('vy', '=f4'), ('vz', '=f4'),
     ('rho', '=f4'), ('temp', '=f4'), ('hsmooth', '=f4'), ('metals', '=f4'), ('phi', '=f4')])
dark_type = np.dtype(
    [('mass', '=f4'), ('x', '=f4'), ('y', '=f4'), ('z', '=f4'), ('vx', '=f4'), ('vy', '=f4'), ('vz', '=f4'),
     ('eps', '=f4'), ('phi', '=f4')])
star_type = np.dtype(
    [('mass', '=f4'), ('x', '=f4'), ('y', '=f4'), ('z', '=f4'), ('vx', '=f4'), ('vy', '=f4'), ('vz', '=f4'),
     ('metals', '=f4'), ('tform', '=f4'), ('eps', '=f4'), ('phi', '=f4')])

fig = plt.figure()
ax = plt.axes()

# input parameters
pathName = argv[1]
projectName = argv[2]

skip = 1
# nsteps = int(argv[4])


kB = 1.38064852e-23  # Boltzmann constant in SI units
u = 1.660538921e-27  # atomic mass unit in kg
M = 3.90e34  # code unit mass in kg
L = 6.17e17  # code unit length in m
T = 3e14  # code unit time in s (T = L^3/(G*M))
RHO = M / (L ** 3)  # code unit density in kg/m^3
# convert from kg/m^3 to amu/cm^3
RHO *= 6.022e26 * 1e-6
L *= 3.240756e-17  # convert from m to pc
temp = int(argv[5])

print(temp)

if len(argv) < 4:
    mode = 'density'
else:
    mode = argv[3]
print(mode)

mean = True

if temp == 9:
    dt = 0.01
elif temp == 14400:
    dt = 0.00025
else:
    print("Specify Timestep")


def plot():
    for i in (0, 25, 50, 75, 100):
        t = 1.0 + dt * i
        colors = plt.cm.jet(np.linspace(0,1, 100))
        # for coolingName in ["nocool", "grackle", "kwh", "kwh_heat"]:
        #     print("")
        #     print("Processing frame %s/%s_%s.%s" % (pathName, projectName, coolingName, str(i*s kip).zfill(5)))
        #     try:
        #         tipsy = open("%s/%s_%s.%s" % (pathName, projectName, coolingName, str(i*skip).zfill(5)), 'rb')
        #     except:
        #         print("Error: file not found")
        #         # ax.set_title('STEP %s'%str(i).zfill(5) + ' TIME {:e}s'.format((t-1.0)*T))
        #         return
        #
        #     print("Reading file")
        #     header = np.fromfile(tipsy, dtype=header_type, count=1)
        #     header = dict(zip(header_type.names, header[0]))
        #     # header['N']     //= skip
        #     # header['Ngas']  //= skip
        #     # header['Ndark'] //= skip
        #     # header['Nstar'] //= skip
        #     gas = np.fromfile(tipsy, dtype=gas_type, count=header['Ngas'])
        #     gas = pd.DataFrame(gas, columns=gas.dtype.names)
        #     dark = np.fromfile(tipsy, dtype=dark_type, count=header['Ndark'])
        #     dark = pd.DataFrame(dark, columns=dark.dtype.names)
        #     star = np.fromfile(tipsy, dtype=star_type, count=header['Nstar'])
        #     star = pd.DataFrame(star, columns=star.dtype.names)
        #     tipsy.close()
        #
        #     t = header['time']
        #     if t < 1.0:
        #         t = 1.0
        #
        #     # sort gas particles by density so that those with highest density are plotted on top
        #     gas.sort_values(by=['rho'], ascending=True, inplace=True)
        #
        #     print("Plotting")
        #
        #     # plot particle x-position vs. density per default, or vs. pressure if mode == "pressure"
        #     # if mode == "density":
        #     #     ax.scatter(gas['x'] * L, gas['rho'] * RHO, s=0.5, edgecolors="none")
        #     # elif mode == "pressure":
        #     #     ax.scatter(gas['x'] * L, gas['temp'] * gas['rho'] * kB / u * RHO, s=0.5, edgecolors="none")
        #     # else:
        #     #     ax.scatter(gas['x'] * L, gas['rho'] * RHO, s=0.5, edgecolors="none")
        #
        #     if mean:
        #         # plot mean density or pressure
        #         meanrho = np.zeros(1000)
        #         meanPr = np.zeros(len(meanrho))
        #         meanT = np.zeros(len(meanrho))
        #         dx = 1.0 / len(meanrho)
        #         for j in range(len(meanrho)):
        #             meanrho[j] = np.mean(gas[(-0.5 + dx * j < gas['x']) & (gas['x'] < -0.5 + dx * (j + 1))]['rho'])
        #             meanPr[j] = np.mean(gas[(-0.5 + dx * j < gas['x']) & (gas['x'] < -0.5 + dx * (j + 1))]['temp']) * meanrho[j]
        #             meanT[j] = np.mean(gas[(-0.5 + dx * j < gas['x']) & (gas['x'] < -0.5 + dx * (j + 1))]['temp'])
        #
        #         # x = np.linspace(-0.5 + dx / 2, 0.5 - dx / 2, len(meanrho))
        #         # xnew = np.linspace(-0.5 + dx / 2, 0.5 - dx / 2, 10*len(meanrho))
        #         # meanrho = np.interp(xnew, x, meanrho)
        #         # meanPr = np.interp(xnew, dx, meanPr)
        #         # meanT = np.interp(xnew, dx, meanT)
        #
        #         if mode == "density":
        #             ax.plot(np.linspace(-0.5 + dx / 2, 0.5 - dx / 2, len(meanrho)) * L, meanrho * RHO, linewidth=0.75,
        #                     label=coolingName)
        #         elif mode == "pressure":
        #             ax.plot(np.linspace(-0.5 + dx / 2, 0.5 - dx / 2, len(meanPr)) * L, meanPr * kB / u * RHO,
        #                     linewidth=0.75, label=coolingName)
        #         elif mode == "temperature":
        #             ax.plot(np.linspace(-0.5 + dx / 2, 0.5 - dx / 2, len(meanT)) * L, meanT, linewidth=0.75,
        #                     label=coolingName)
        #         else:
        #             ax.plot(np.linspace(-0.5 + dx / 2, 0.5 - dx / 2, len(meanrho)) * L, meanrho * RHO, linewidth=0.75,
        #                     label=coolingName)

            # calculate avg temperature over all
            # if coolingName == "nocool":
            #     temp_avg = np.mean(meanT)
            #     print(temp_avg)

        # analytic solution for Sod shock tube
        if "sod" in projectName:
            print("Computing analytic solution for time %f" % (t - 1.0))
            (xgrid, PrE, uE, rhoE, machE, entropy_E, tempE) = sod.sod((t - 1.0) * T, temp)
            # rhoE[0,:] *= 6.022e26 * 1e-6
            # xgrid3.240756e-17
            if mode == "density":
                ax.plot(xgrid[0, :], rhoE[0, :], 'k', linewidth=0.75, label=r'%s' % str(i))
                # print(f"Simulation: {meanrho*RHO}")
                # print(f"Analytic: {rhoE[0,:]}")
            elif mode == "pressure":
                ax.plot(xgrid[0, :], PrE[0, :], c=colors[int(i/2)], linewidth=0.75, label=r'%s' % str(i))
                # print(f"Simulation: {meanPr*kB/u*RHO}")
                # print(f"Analytic: {PrE[0,:]}")
            elif mode == "temperature":
                ax.plot(xgrid[0,:], tempE[0,:], c=colors[int(i/2)], linewidth=0.75, label=r'%s' % str(i))

    if "sod" in projectName:
        ax.set_xlim(-0.25 * L, 0.25 * L)
    elif "peq" in projectName:
        ax.set_xlim(-0.25 * L, 0.25 * L)
    else:
        ax.set_xlim(-0.5 * L, 0.5 * L)
    ax.set_xlabel(r'x [$pc$]')

    if mode == "density":
        ax.set_ylim(0.0, 2 * RHO)
        ax.set_ylabel(r'density [$AMU/cm^3$]')
    elif mode == "pressure":
        ax.set_ylim(0.0, 2 * temp * kB / u * RHO)
        ax.set_ylabel(r'pressure [$Pa$]')
    elif mode == "temperature":
        ax.set_ylim(0.0, 1.5 * temp)
        ax.set_ylabel(r'temperature [$K$]')
    else:
        ax.set_ylim(0.0, 2 * RHO)
        ax.set_ylabel(r'density [$kg/m^3$]')

    ax.legend(loc='upper right', title="Time step")
    time = (t - 1.0) * T
    # convert from seconds to years
    time *= 3.1689e-8
    # ax.set_title('STEP %s' % str(i).zfill(5) + ' TIME {:e}yr'.format(time))
    ax.set_title(r'Analytic Solution of Sod Shock Tube')


i = int(argv[4])
if i == -1:
    plot()
    plt.savefig("%s/plots/plot_%s_%s_analytical_evolution.png" % (pathName, projectName, mode), dpi=300)
# plt.show()
