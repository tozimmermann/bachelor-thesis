#include <fstream>
#include <sstream>
#include <iostream>

struct Header {
    double time = 1;
    int n;
    int dims = 3;
    int ngas;
    int ndark = 0;
    int nstar = 0;
    int pad;
};

struct Gas {
    float mass;
    float x;
    float y;
    float z;
    float vx;
    float vy;
    float vz;
    float rho;
    float temp;
    float hsmooth;
    float metals;
    float phi;
};

//struct Dark {
//    float mass;
//    float x;
//    float y;
//    float z;
//    float vx;
//    float vy;
//    float vz;
//    float eps;
//    float phi;
//};
//
//struct Star {
//    float mass;
//    float x;
//    float y;
//    float z;
//    float vx;
//    float vy;
//    float vz;
//    float metals;
//    float tform;
//    float eps;
//    float phi;
//};

int main(int argc, char *argv[]) {
    std::cout << "Converting " << argv[1] << ".txt into tipsy file format..." << std::endl;
    std::string exec_name = argv[1];
    std::cout << exec_name << std::endl;
    std::ifstream inFile(exec_name + ".txt");
    // add ".tipsy" to file name and open it for writing
    std::ofstream outFile(exec_name + ".tipsy", std::ios::binary);

    // Read and parse the header
    std::string headerLine;
    std::getline(inFile, headerLine);
    std::istringstream headerStream(headerLine);
    Header header;
    headerStream >> header.time >> header.n >> header.dims >> header.ngas >> header.ndark >> header.nstar >> header.pad;

    // Write the header to the binary file
    outFile.write(reinterpret_cast<const char*>(&header), sizeof(Header));

    // For each remaining line in the text file
    std::string line;
    while (std::getline(inFile, line)) {
        // Parse the gas particle data
        std::istringstream lineStream(line);
        Gas gas;
        lineStream >> gas.mass >> gas.x >> gas.y >> gas.z >> gas.vx >> gas.vy >> gas.vz >> gas.rho >> gas.temp >> gas.hsmooth >> gas.metals >> gas.phi;

        // Write the gas particle data to the binary file
        outFile.write(reinterpret_cast<const char*>(&gas), sizeof(Gas));
    }

    inFile.close();
    outFile.close();

    return 0;
}