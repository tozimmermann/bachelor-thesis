import numpy as np
import pandas as pd
from sys import argv, exit
from matplotlib import pyplot as plt
import matplotlib.animation as animation
import sod

fig = plt.figure()
ax = plt.axes()

skip = 1
nsteps = 25
dt = 0.001

kB = 1.38064852e-23  # Boltzmann constant in SI units
u = 1.660538921e-27  # atomic mass unit in kg
M = 3.90e34  # code unit mass in kg
L = 6.17e17  # code unit length in m
T = 3e14  # code unit time in s (T = L^3/(G*M))
RHO = M / (L ** 3)  # code unit density in kg/m^3
# convert from kg/m^3 to amu/m^3
RHO *= 6.022e26 * 1e-6
L *= 3.240756e-17  # convert from m to pc
temp = 16000

avg_temp = np.zeros(nsteps)

for i in range(nsteps):
    print(i)
    t = dt * i
    print("Computing analytic solution for time %f" % t)
    (xgrid, PrE, uE, rhoE, machE, entropy_E, tempE) = sod.sod(t * T, temp)
    avg_temp[i] = np.mean(tempE)
    # if i % 5 == 0:
    #     label = "Time = %f" % t
    #     ax.plot(xgrid[0, :], tempE[0, :], label=label)

# ax.plot(np.arange(nsteps) * dt * T / (3600 * 24 * 365), avg_temp)
# ax.set_xlabel("Time [yr]")
# ax.set_ylabel("Temperature [K]")
# ax.set_title("Average temperature in Sod shock tube over time")
# # ax.legend()
# plt.show()

plt.cla()
ax.plot(np.arange(nsteps) * dt * T / (3600 * 24 * 365), (avg_temp / temp - 1) * 100, label="T = 16000 K")
ax.set_xlabel("Time [yr]")
ax.set_ylabel(r"Relative Change $\frac{T(t)}{T(0)}$ [%]")
# ax.set_title("Average Temperature in Sod shock tube over time (relative change)")
ax.legend()
plt.savefig("sod_temp_test.png")
plt.show()


