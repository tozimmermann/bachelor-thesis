import numpy as np
import pandas as pd
from sys import argv, exit
from matplotlib import pyplot as plt


# This script plots the energy level diagram for hydrogen
# The y-axis are the energy levels in eV as well as in form of the quantum numbers

# Constants
Rydberg = 13.6  # eV
n = np.arange(1, 10)
E = -Rydberg / n ** 2

# Create a figure and an axis
fig, ax = plt.subplots()

# Loop over the energy levels
for i in range(len(n)):
    # Plot a horizontal line for each energy level
    ax.axhline(y=E[i], color='b', linewidth=0.5)
    # Add the energy level label on the left
    # ax.text(0.5, E[i], f'{E[i]:.2f} eV', va='center', ha='right', backgroundcolor='w')
    # Add the quantum number label on the right
    if i < 5:
        ax.text(0.5, E[i], f'n={n[i]}', va ='center', ha='right', backgroundcolor='w')

ax.axhline(y=0, color='k', linewidth=0.5, linestyle='--')
ax.text(0.59, 0, 'Ionisation level', va='center', ha='right', backgroundcolor='w')

# Set the y-axis to logarithmic
ax.set_yscale('symlog')
ax.set_yticks(list(E[:5]) + [0], minor=False)
ax.set_yticklabels(['{:.2f}'.format(e) for e in list(E[:5]) + [0]])


# Set the title and the y-label
# ax.set_title('Energy Level Diagram for Hydrogen')
ax.set_ylabel('Energy (eV)')

# Hide the right, top and bottom spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)

# Hide the x-axis
ax.get_xaxis().set_visible(False)

# save the figure
plt.savefig('energy_level_diagram_hydrogen.pdf')

# Display the plot
plt.show()